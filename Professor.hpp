#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include "Pessoa.hpp"
#include <string>
#include <iostream>

using namespace std;

class Professor : public Pessoa {


private:

	string formacao;
	float salario;
	string sala;

public:

	Professor();
	~Professor();

	string getFormacao();
	void setFormacao(string formacao);

	float getSalario();
	void setSalario(float salario);

	string getSala();
	void setSala(string sala);

	void imprimeDadosProfessor();


};

#endif 
