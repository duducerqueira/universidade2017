#include "Pessoa.hpp"
#include <iostream>
#include <string>

using namespace std;


Pessoa::Pessoa(){
	nome = "";
	matricula = "";
	idade = 0;
	sexo = "";
	telefone = "";
}

Pessoa::Pessoa(string nome, string telefone, int idade){
	setNome(nome);
	setTelefone(telefone);
	setIdade(idade);

}

Pessoa::~Pessoa(){

}

string Pessoa::getNome(){
	return nome;
}
void Pessoa::setNome(string nome){
	this-> nome = nome;
}
string Pessoa::getMatricula(){
        return matricula;
}
void Pessoa::setMatricula(string matricula){
        this-> matricula = matricula;
}
int Pessoa::getIdade(){
        return idade;
}
void Pessoa::setIdade(int idade){
        this-> idade = idade;
}
string Pessoa::getSexo(){
        return sexo;
}
void Pessoa::setSexo(string sexo){
        this-> sexo = sexo;
}
string Pessoa::getTelefone(){
        return telefone;
}
void Pessoa::setTelefone(string telefone){
        this-> telefone = telefone;
}
void Pessoa::imprimeDados(){

	cout << "Dados do objeto pessoa -----------" << endl;
	cout << "Nome: " << getNome() << endl;
	cout << "Matricula: " << getMatricula() << endl;
	cout << "Telefone: " << getTelefone() << endl;
	cout << "Idade: " << getIdade() << endl;
	cout << "Sexo: " << getSexo() << endl;
}

