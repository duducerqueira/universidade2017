#include "Aluno.hpp"
#include <string>
#include <iostream>

using namespace std;

Aluno::Aluno(){

   setNome("");
   setMatricula("");
   setIdade(0);
   setSexo("");
   setTelefone("");
   setIra(5.0);
   setSemestre(1);
   setCurso("Engenharias");

}

Aluno::~Aluno(){

}

	float Aluno::getIra(){
	   return ira;
	}

	void Aluno::setIra(float ira){
	   this->ira = ira;
	}

	int Aluno::getSemestre(){
	   return semestre;
	}

	void Aluno::setSemestre(int semestre){
	   this->semestre = semestre;
	}

	string Aluno::getCurso(){
	   return curso;
	}

	void Aluno::setCurso(string curso){
	   this->curso = curso;
	}
	
	void Aluno::imprimeDadosAluno(){
		
	   cout << "Dados do objeto aluno -----------" << endl;
           cout << "Nome: " << getNome() << endl;
           cout << "Matricula: " << getMatricula() << endl;
           cout << "Telefone: " << getTelefone() << endl;
           cout << "Idade: " << getIdade() << endl;
           cout << "Sexo: " << getSexo() << endl;
	   cout << "Ira: " << getIra() << endl;
	   cout << "Semestre: " << getSemestre() << endl;
	   cout << "Curso: " << getCurso() << endl;



	}
