#ifndef ALUNO_HPP
#define ALUNO_HPP

#include "Pessoa.hpp"
#include <string>
#include <iostream>

using namespace std;

class Aluno : public Pessoa {

private:

	float ira;
	int semestre;
	string curso;

public:

	Aluno();
	~Aluno();

	float getIra();
	void setIra(float ira);

	int getSemestre();
	void setSemestre(int semestre);

	string getCurso();
	void setCurso(string curso);

	void imprimeDadosAluno();
};


#endif
