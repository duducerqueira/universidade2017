#include "Professor.hpp"

#include <string>
#include <iostream>

using namespace std;

Professor::Professor(){
   
   setNome("");
   setMatricula("");
   setIdade(0);
   setSexo("");
   setTelefone("");
   setFormacao("");
   setSalario(0.0);
   setSala("");

}

Professor::~Professor(){

}

   string Professor:: getFormacao(){
	return formacao;
   }

   void Professor:: setFormacao(string formacao){
	this->formacao = formacao;
   }
   float Professor:: getSalario(){
        return salario;
   }

   void Professor:: setSalario(float salario){
        this->salario = salario;
   }
   string Professor:: getSala(){
        return sala;
   }

   void Professor:: setSala(string sala){
        this->sala = sala;
   }

	   void Professor::imprimeDadosProfessor(){
           cout << "Dados do objeto professor -----------" << endl;
           cout << "Nome: " << getNome() << endl;
           cout << "Matricula: " << getMatricula() << endl;
           cout << "Telefone: " << getTelefone() << endl;
           cout << "Idade: " << getIdade() << endl;
           cout << "Sexo: " << getSexo() << endl;
           cout << "Formacao: " << getFormacao() << endl;
	   cout << "Salario: " << getSalario() << endl;
	   cout << "Sala: " << getSala() << endl;

   }

