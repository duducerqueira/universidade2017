#include <iostream>
#include "Pessoa.hpp"
#include "Aluno.hpp"
#include "Professor.hpp"

using namespace std;

int main(int argc, char ** argv){

	Pessoa pessoa_1;
	Pessoa pessoa_2("Maria", "555-1234567", 20);
	
	Pessoa * pessoa_3;
	pessoa_3 = new Pessoa();

	Pessoa * pessoa_4;
	pessoa_4 = new Pessoa("Dudu", "444-525252", 25);

	Aluno * aluno_1 = new Aluno();

	Professor * professor_1 = new Professor();
	/*	
	pessoa_1.setNome("Eduardo");
	pessoa_1.setMatricula("13/0108448");
	pessoa_1.setIdade(20);
	pessoa_1.setSexo("Masculino");
	pessoa_1.setTelefone("61-33849406");

	pessoa_3->setNome("Paulo");
        pessoa_3->setMatricula("13/0102030");
        pessoa_3->setIdade(20);
        pessoa_3->setSexo("Masculino");
        pessoa_3->setTelefone("61-33849406");



	pessoa_1.imprimeDados();
	pessoa_2.imprimeDados();
	pessoa_3->imprimeDados();
	pessoa_4->imprimeDados();
	*/

	//cout << "Curso do aluno: " << aluno_1->getCurso() << endl;
	
	//aluno_1->imprimeDadosAluno();
	professor_1->imprimeDadosProfessor();

	delete(pessoa_3);
        delete(pessoa_4);
	delete(aluno_1);
	delete(professor_1);

   return 0;
}
